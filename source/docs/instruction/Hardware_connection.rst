硬件连接
=============

本文档主要介绍下载和烧录固件、发送 AT 命令和接收 AT 响应所需要的硬件以及硬件之间该如何连接。

对于不同系列的模组，AT 默认固件所支持的命令会有所差异。具体可参考 :doc:`other/firmware_differences`
      


Ai-WB2 系列
------------------

.. list-table::
    :header-rows: 1

    * - USB转TTL
      - Ai-WB2系列
      
    * - VCC
      - 3.3V
          
    * - GND
      - GND
      
    * - TXD
      - RXD
     
    * - RXD
      - TXD
      


BW16模组（ 瑞昱RTL8720DN方案 ）
--------------------------------------

.. list-table::
    :header-rows: 1

    * - USB转TTL
      - BW16
      
    * - VCC
      - 3.3V
          
    * - GND
      - GND
      
    * - TXD
      - RXD
     
    * - RXD
      - TXD
      

TB泰凌蓝牙系列
------------------

TB-01接线示意表


.. list-table::
    :header-rows: 1


    * - USB转TTL
      - TB-01
      
    * - VCC
      - 3.3V
          
    * - GND
      - GND
      
    * - TXD
      - PB1
     
    * - RXD
      - PB0
      
TB-02接线示意表

.. list-table::
    :header-rows: 1


    * - USB转TTL
      - TB-02
      
    * - VCC
      - 3.3V
          
    * - GND
      - GND
      
    * - TXD
      - RX0
     
    * - RXD
      - TX0


TB-03F接线示意表

.. list-table::
    :header-rows: 1


    * - USB转TTL
      - TB-03F
      
    * - VCC
      - 3.3V
          
    * - GND
      - GND
      
    * - TXD
      - PA0
     
    * - RXD
      - PB1


TB-04接线示意表

.. list-table::
    :header-rows: 1


    * - USB转TTL
      - TB-04
      
    * - VCC
      - 3.3V
          
    * - GND
      - GND
      
    * - TXD
      - RXD
     
    * - RXD
      - TXD



PB奉加蓝牙系列
------------------

PB-01/PB-02接线示意表

.. list-table::
    :header-rows: 1


    * - USB转TTL
      - PB-01/PB-02
      
    * - VCC
      - 3.3V
          
    * - GND
      - GND
      
    * - TXD
      - RXD
     
    * - RXD
      - TXD


PB-03/PB-03F/PB-03M接线示意表

.. list-table::
    :header-rows: 1


    * - USB转TTL
      - PB-03/PB-03F/PB-03M
      
    * - VCC
      - 3.3V
          
    * - GND
      - GND
      
    * - TXD
      - P10
     
    * - RXD
      - P9
