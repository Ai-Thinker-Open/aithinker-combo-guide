最新固件下载
=============

这里汇集了各个硬件模组的Combo-AT固件的下载链接；


Ai-WB2系列
------------------
链接： `点击链接 <https://aithinker.readthedocs.io/zh_CN/latest/docs/taobao/BL/index.html>`_

BW16模组（ 瑞昱RTL8720DN方案 ）
------------------------------------------------
链接： `点击链接 <https://docs.ai-thinker.com/lib/exe/fetch.php?tok=b4fb3e&media=https%3A%2F%2Fdocs.ai-thinker.com%2F_media%2Frtl8720%2Fbw16_combo_soft_v4.18_p5.0.0.zip>`_

TB泰凌蓝牙系列
------------------
链接： `点击链接 <https://aithinker.readthedocs.io/zh_CN/latest/docs/taobao/ble/index.html#tb>`_


PB奉加蓝牙系列
------------------
链接： `点击链接 <https://aithinker.readthedocs.io/zh_CN/latest/docs/taobao/ble/index.html>`_
